package com.example.demo.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Album;
import com.example.demo.Model.Artist;

@Service
public class ArtistService {

    @Autowired
    AlbumService albumService;

    Artist NooPhuocThinhVol1 = new Artist(10,"Noo Phước Thịnh");
    Artist VuCatTuongVol2 = new Artist(11,"Vũ Cát Tường");
    Artist BaoAnhVol3 = new Artist(12,"Bảo Anh");

    public ArrayList<Artist> getArtistList() {
        ArrayList<Artist> artistList = new ArrayList<>();
        
        NooPhuocThinhVol1.setAlbums(albumService.getAlbumNooPhuocThinh());
        VuCatTuongVol2.setAlbums(albumService.getAlbumVuCatTuong());
        BaoAnhVol3.setAlbums(albumService.getAlbumBaoAnh());

        artistList.add(NooPhuocThinhVol1);
        artistList.add(VuCatTuongVol2);
        artistList.add(BaoAnhVol3);
    
        return artistList;
    }
}
